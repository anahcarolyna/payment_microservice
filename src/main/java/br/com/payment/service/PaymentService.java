package br.com.payment.service;

import br.com.payment.clients.CreditCardClient;
import br.com.payment.models.Payment;
import br.com.payment.models.dto.CreditCard;
import br.com.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CreditCardClient creditCardClient;

    public Payment create(Payment payment) {
        CreditCard creditCard = creditCardClient.getById(payment.getCreditCard());

        return paymentRepository.save(payment);
    }

    public List<Payment> findAllByCreditCard(Long creditCardId) {
        return paymentRepository.findAllByCreditCard(creditCardId);
    }
}
