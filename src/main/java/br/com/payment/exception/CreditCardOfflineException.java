package br.com.payment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Serviço indisponível, tente daqui alguns minutos.")
public class CreditCardOfflineException extends RuntimeException{
}
