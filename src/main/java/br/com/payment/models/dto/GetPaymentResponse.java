package br.com.payment.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class GetPaymentResponse {
    private long id;

    @JsonProperty("cartao_id")
    private long creditCard_id;

    @JsonProperty("descricao")
    private  String description;

    @JsonProperty("valor")
    private BigDecimal value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreditCard_id() {
        return creditCard_id;
    }

    public void setCreditCard_id(long creditCard_id) {
        this.creditCard_id = creditCard_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
