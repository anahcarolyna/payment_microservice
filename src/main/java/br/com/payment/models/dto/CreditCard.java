package br.com.payment.models.dto;

public class CreditCard {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
