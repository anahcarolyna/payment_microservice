package br.com.payment.models.dto;

import br.com.payment.models.Payment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaymentMapper {

    public Payment toPayment(CreatePaymentRequest createPaymentRequest) {
        Payment payment = new Payment();
        payment.setDescription(createPaymentRequest.getDescription());
        payment.setValue(createPaymentRequest.getValue());
        payment.setCreditCard(createPaymentRequest.getCreditCard());


        return payment;
    }

    public CreatePaymentResponse toCreatePaymentResponse(Payment payment) {
        CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();

        createPaymentResponse.setId(payment.getId());
        createPaymentResponse.setDescription(payment.getDescription());
        createPaymentResponse.setCreditCard(payment.getCreditCard());
        createPaymentResponse.setValue(payment.getValue());

        return createPaymentResponse;
    }

    public List<GetPaymentResponse> toGetPaymentResponse(List<Payment> paymentList) {
        List<GetPaymentResponse> paymentResponseList = new ArrayList<>();


        for (Payment payment: paymentList) {
            GetPaymentResponse getPaymentResponse = new GetPaymentResponse();
            getPaymentResponse.setId(payment.getId());
            getPaymentResponse.setDescription(payment.getDescription());
            getPaymentResponse.setCreditCard_id(payment.getCreditCard());
            getPaymentResponse.setValue(payment.getValue());

            paymentResponseList.add(getPaymentResponse);
        }

        return paymentResponseList;
    }
}
