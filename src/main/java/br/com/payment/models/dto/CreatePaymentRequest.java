package br.com.payment.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CreatePaymentRequest {
    @NotNull(message = "É obrigatório o preenchimento da cartão")
    @JsonProperty("cartao_id")
    private long creditCard;

    @Size(min = 5, max = 100, message = "A descrição deve ter entre 5 a  100 caracteres")
    @NotNull(message = "É obrigatório o preenchimento da descrição")
    @JsonProperty("descricao")
    private  String description;

    @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto")
    @NotNull(message = "É obrigatório o preenchimento do valor")
    @JsonProperty("valor")
    private BigDecimal value;

    public long getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(long creditCard) {
        this.creditCard = creditCard;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

}
