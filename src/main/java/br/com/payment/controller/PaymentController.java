package br.com.payment.controller;

import br.com.payment.models.Payment;
import br.com.payment.models.dto.CreatePaymentRequest;
import br.com.payment.models.dto.CreatePaymentResponse;
import br.com.payment.models.dto.GetPaymentResponse;
import br.com.payment.models.dto.PaymentMapper;
import br.com.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentMapper mapper;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public CreatePaymentResponse create(@RequestBody @Valid CreatePaymentRequest createPaymentRequest) {
        Payment payment = mapper.toPayment(createPaymentRequest);

        return mapper.toCreatePaymentResponse(paymentService.create(payment));
    }

    @GetMapping("/pagamentos/{creditCardId}")
    public List<GetPaymentResponse> findAllByCreditCard(@PathVariable Long creditCardId) {
        return mapper.toGetPaymentResponse(paymentService.findAllByCreditCard(creditCardId));
    }
}
