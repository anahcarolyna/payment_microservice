package br.com.payment.repository;

import br.com.payment.models.Payment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {
    List<Payment> findAllByCreditCard(Long creditCardId);
}

