package br.com.payment.clients;

import br.com.payment.exception.InvalidCreditCardException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CreditCardClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return  new InvalidCreditCardException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
