package br.com.payment.clients;

import br.com.payment.models.dto.CreditCard;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CreditCard", configuration = CreditCardClientConfiguration.class)
public interface CreditCardClient {

    @GetMapping("/cartao/cartao/id/{id}")
    CreditCard getById(@PathVariable Long id);
}
