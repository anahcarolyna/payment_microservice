package br.com.payment.clients;

import br.com.payment.exception.CreditCardOfflineException;
import br.com.payment.models.dto.CreditCard;

public class CreditCardFallback implements CreditCardClient {

    @Override
    public CreditCard getById(Long id) {

        throw new CreditCardOfflineException();
    }
}
